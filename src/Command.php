<?php
namespace Migrator;

use Migrator\Utils\Arr;
use Migrator\Utils\Path;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class Command extends \Symfony\Component\Console\Command\Command
{
	protected $commandName;
	protected $commandDescription;
	protected $commandHelp;
	
	
	/** @var InputInterface */
	protected $input;
	
	/** @var OutputInterface */
	protected $output;
	
	
	/** @var Database */
	private $database;
	
	/** @var string */
	protected $cwd;
	
	/** @var array */
	protected $config;
	
	
	/**
	 * 
	 */
	protected function configure()
	{
		if (! $this->getName()) {
			$this->setName($this->commandName);
		}
		
		$this->setDescription($this->commandDescription);
		$this->setHelp($this->commandHelp);
		
		$this->cwd = getcwd();
	}
	
	
	/**
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 */
	protected function initialize(InputInterface $input, OutputInterface $output)
	{
		if (! $this->config) {
			throw new \InvalidArgumentException('Missing configuration: '.$this->getName());
		}
	}
	
	
	/**
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 *
	 * @return void|null|int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->input = $input;
		$this->output = $output;
		
		$this->handle();
	}
	
	
	abstract protected function handle();
	
	
	/**
	 * @param array $config
	 */
	public function setConfig(array $config)
	{
		$this->config = $config;
	}
	
	
	/**
	 * @return Database
	 */
	protected function getDatabase()
	{
		if (! $this->database) {
			$driver   = Arr::get($this->config, 'database_driver');
			$host     = Arr::get($this->config, 'database_host');
			$name     = Arr::get($this->config, 'database_name');
			$user     = Arr::get($this->config, 'database_user');
			$password = Arr::get($this->config, 'database_password');
			$port     = Arr::get($this->config, 'database_port');
			
			$this->database = Database::instance();
			$this->database->setup($driver, $host, $name, $user, $password, $port);
		}
		
		return $this->database;
	}
	
	
	/**
	 * @return Migration[]
	 */
	public function getLocalMigrations()
	{
		$migrationDir = Path::pretty(__DIR__.'/../migrations');
		
		$files = scandir($migrationDir);
		Arr::filterFiles($files);
		
		/** @var Migration[] $migrations */
		$migrations = [];
		foreach ($files as $file) {
			$migration = Migration::createFromFile(Path::join($migrationDir, $file));
			$migrations[$migration->getId()] = $migration;
		}
		
		ksort($migrations);
		
		return $migrations;
	}
	
	
	/**
	 * @return Migration[]
	 */
	public function getRemoteMigrations()
	{
		$migrationDir = Path::pretty(__DIR__.'/../migrations');
		$result = $this->getDatabase()->select("SELECT * FROM changelog ORDER BY id");
		
		/** @var Migration[] $migrations */
		$migrations = [];
		if ($result) {
			foreach ($result as $row) {
				$migration = Migration::createFromRow($row, $migrationDir);
				$migrations[$migration->getId()] = $migration;
			}
			
			ksort($migrations);
		}
		
		return $migrations;
	}
	
	
	/**
	 * @return Migration[]
	 */
	public function getLocalAndRemoteMigrations()
	{
		$locales = $this->getLocalMigrations();
		$remotes = $this->getRemoteMigrations();
		
		foreach ($remotes as $remote) {
			$locales[$remote->getId()] = $remote;
		}
		
		ksort($locales);
		
		return $locales;
	}
	
	
	/**
	 * @return Migration[]
	 */
	public function getMigrationsToUp()
	{
		$locales = $this->getLocalMigrations();
		$remotes = $this->getRemoteMigrations();
		
		foreach ($remotes as $remote) {
			unset($locales[$remote->getId()]);
		}
		
		ksort($locales);
		
		return $locales;
	}
	
	
	/**
	 * @return Migration[]
	 */
	public function getMigrationsToDown()
	{
		$remotes = $this->getRemoteMigrations();
		ksort($remotes);
		
		$remotes = array_reverse($remotes, true);
		
		return $remotes;
	}
	
	
	public function executeUpMigration(Migration $migration)
	{
		$this->getDatabase()->transaction();
		
		$this->getDatabase()->run($migration->getSqlUp());
		
		$this->saveToChangelog($migration);
		$this->getDatabase()->commit();
	}
	
	
	public function executeDownMigration(Migration $migration)
	{
		$this->getDatabase()->transaction();
		
		$this->getDatabase()->run($migration->getSqlDown());
		
		$this->deleteFromChangelog($migration);
		$this->getDatabase()->commit();
	}
	
	
	public function saveToChangelog(Migration $migration)
	{
		$migration->setAppliedAt(new \DateTime);
		
		$sql = "INSERT INTO changelog
          (id, applied_at, description)
          VALUES
          ({$migration->getId()},'{$migration->getAppliedAt()->format('Y-m-d H:i:s')}','{$migration->getDescription()}');
        ";
		$result = $this->getDatabase()->insert($sql);
		
		if (! $result) {
			throw new \RuntimeException("changelog table has not been initialized");
		}
	}
	
	
	public function deleteFromChangelog(Migration $migration)
	{
		$this->getDatabase()->run("DELETE FROM changelog WHERE id = {$migration->getId()}");
	}
}
