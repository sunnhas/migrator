<?php
namespace Migrator;

class Database
{
	/** @var null|static */
	protected static $instance = null;
	
	/** @var \PDO */
	protected $connection;
	
	
	/** @var array */
	protected $supportedDrivers = ['mysql', 'sqlite', 'sqlite2'];
	
	
	protected function __construct() {}
	
	
	/**
	 * @return static
	 */
	public static function instance()
	{
		if (! self::$instance) {
			self::$instance = new static();
		}
		
		return self::$instance;
	}
	
	
	/**
	 * @param string   $driver
	 * @param string   $host
	 * @param string   $name
	 * @param string   $user
	 * @param string   $password
	 * @param null|int $port
	 *
	 * @return $this
	 */
	public function setup($driver, $host, $name, $user, $password, $port = null)
	{
		if ($this->connection) {
			return $this;
		}
		
		$this->validateDriver($driver);
		
		if (in_array($driver, ['sqlite', 'sqlite2'])) {
			$dsn = $driver.':'.$host;
		} else {
			$dsn = $driver.':host='.$host.';dbname='.$name;
		}
		
		if ($port !== null) {
			$dsn .= ';port='.$port;
		}
		
		$this->connection = new \PDO($dsn, $user, $password);
		$this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		
		return $this;
	}
	
	
	/**
	 * @param string $query
	 * @param array  $params
	 *
	 * @return \PDOStatement
	 */
	public function run($query, array $params = [])
	{
		$query = $this->connection->prepare($query);
		
		foreach ($params as $id => $param) {
			$query->bindParam($id, $params);
		}
		
		$query->execute();
		
		return $query;
	}
	
	
	/**
	 * @param string $query
	 * @param array  $params
	 *
	 * @return array
	 */
	public function select($query, array $params = [])
	{
		$query = $this->run($query, $params);
		
		return $query->fetchAll(\PDO::FETCH_ASSOC);
		
	}
	
	
	/**
	 * @param string $query
	 * @param array  $params
	 *
	 * @return null|string
	 */
	public function insert($query, array $params = [])
	{
		if ($this->run($query, $params)) {
			return $this->connection->lastInsertId();
		} else {
			return null;
		}
	}
	
	
	/**
	 * @return bool
	 */
	public function transaction()
	{
		return $this->connection->beginTransaction();
	}
	
	
	/**
	 * @return bool
	 */
	public function commit()
	{
		return $this->connection->commit();
	}
	
	
	/**
	 * @param string $driver
	 */
	protected function validateDriver($driver)
	{
		if (! in_array($driver, \PDO::getAvailableDrivers())) {
			throw new \InvalidArgumentException('Driver "'.$driver.'" is not installed as PHP extension');
		} elseif (! in_array($driver, $this->supportedDrivers)) {
			throw new \InvalidArgumentException('Driver "'.$driver.'" is not supported by '.get_class($this));
		}
	}
	
	
	public function __destruct()
	{
		unset($this->connection);
	}
}
