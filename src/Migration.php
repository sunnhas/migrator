<?php
namespace Migrator;

use Migrator\Utils\Arr;

class Migration
{
	/** @var int */
	protected $id;
	
	/** @var null|\DateTime */
	protected $appliedAt;
	
	/** @var string */
	protected $description;
	
	/** @var string */
	protected $file;
	
	/** @var string */
	protected $sqlUp;
	
	/** @var string */
	protected $sqlDown;
	
	
	protected function __construct()
	{
	}
	
	
	/**
	 * @param string $path
	 *
	 * @return Migration
	 */
	public static function createFromFile($path)
	{
		$filename = basename($path);
		$data = explode('_', $filename);
		
		$migration = new self();
		$migration->setId((int) $data[0]);
		$migration->setAppliedAt(null);
		$migration->setDescription(str_replace('.sql', '', $data[1]));
		$migration->setFile(realpath($path));
		$migration->load();
		
		return $migration;
	}
	
	
	/**
	 * @param array  $data
	 * @param string $migrationsDir
	 *
	 * @return Migration
	 */
	public static function createFromRow(array $data, $migrationsDir)
	{
		$migration = new self();
		$migration->setId(Arr::get($data, 'id'));
		$migration->setAppliedAt(new \DateTime(Arr::get($data, 'applied_at')));
		$migration->setDescription(Arr::get($data, 'description'));
		$migration->setFile($migrationsDir.'/'.$migration->getId().'_'.$migration->getDescription().'.sql');
		$migration->load();
		
		return $migration;
	}
	
	
	/**
	 * @param int $id
	 *
	 * @return Migration
	 */
	public function setId($id)
	{
		$this->id = $id;
		
		return $this;
	}
	
	
	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}
	
	
	/**
	 * @param null|\DateTime $appliedAt
	 *
	 * @return Migration
	 */
	public function setAppliedAt(\DateTime $appliedAt = null)
	{
		$this->appliedAt = $appliedAt;
		
		return $this;
	}
	
	
	/**
	 * @return null|\DateTime
	 */
	public function getAppliedAt()
	{
		return $this->appliedAt;
	}
	
	
	/**
	 * @param string $description
	 *
	 * @return Migration
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		
		return $this;
	}
	
	
	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}
	
	
	/**
	 * @param string $file
	 *
	 * @return Migration
	 */
	public function setFile($file)
	{
		$this->file = $file;
		
		return $this;
	}
	
	
	/**
	 * @return string
	 */
	public function getFile()
	{
		return $this->file;
	}
	
	
	/**
	 * @param string $sqlUp
	 *
	 * @return Migration
	 */
	public function setSqlUp($sqlUp)
	{
		$this->sqlUp = $sqlUp;
		
		return $this;
}
	
	
	/**
	 * @return string
	 */
	public function getSqlUp()
	{
		return $this->sqlUp;
	}
	
	
	/**
	 * @param string $sqlDown
	 *
	 * @return Migration
	 */
	public function setSqlDown($sqlDown)
	{
		$this->sqlDown = $sqlDown;
		
		return $this;
	}
	
	
	/**
	 * @return string
	 */
	public function getSqlDown()
	{
		return $this->sqlDown;
	}
	
	
	/**
	 *
	 */
	public function load()
	{
		$content = file_get_contents($this->getFile());
		
		if (! $content) {
			throw new \RuntimeException('Migration script has no content: '.$this->getFile());
		} elseif (strpos($content, '@UP') === false) {
			throw new \RuntimeException('Migration script has no @UP section: '.$this->getFile());
		} elseif (strpos($content, '@DOWN') === false) {
			throw new \RuntimeException('Migration script has no @DOWN section: '.$this->getFile());
		}
		
		$sql = explode('-- @UP', $content);
		$sqlParts = explode('-- @DOWN', $sql[1]);
		$this->setSqlUp($sqlParts[0])->setSqlDown($sqlParts[1]);
	}
	
	
	/**
	 * @return array
	 */
	public static function toArrayHeaders()
	{
		return [
			'Id',
			'Applied at',
			'Description',
		];
	}
	
	/**
	 * @return array
	 */
	public function toArray()
	{
		return [
			$this->getId(),
			$this->getAppliedAt() ? $this->getAppliedAt()->format('Y-m-d H:i:s') : 'Not applied yet',
			$this->getDescription(),
		];
	}
}
