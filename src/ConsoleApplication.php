<?php
namespace Migrator;

use Migrator\Utils\File;
use Migrator\Utils\Path;
use Symfony\Component\Console\Application;

class ConsoleApplication
{
	const APP_NAME = 'Migrator';
	const APP_VERSION = '0.0.1';
	
	/** @var Application */
	protected $app;
	
	/** @var array */
	protected $config;
	
	
	public function __construct(array $config = [])
	{
		$this->app = new Application(self::APP_NAME, self::APP_VERSION);
		
		$this->loadConfig($config);
		$this->setupCommands();
	}
	
	
	public function run()
	{
		$this->app->run();
	}
	
	
	protected function setupCommands()
	{
		$this->loadCommand(new Command\InitCommand());
		$this->loadCommand(new Command\StatusCommand());
		$this->loadCommand(new Command\CreateCommand());
		$this->loadCommand(new Command\UpCommand());
		$this->loadCommand(new Command\DownCommand());
	}
	
	
	protected function loadCommand(Command $command)
	{
		$command->setConfig($this->config);
		$this->app->add($command);
	}
	
	
	/**
	 * @param array $config
	 */
	protected function loadConfig(array $config = [])
	{
		$cwd = getcwd();
		
		$configFile = [];
		if (Path::exists(Path::join($cwd, 'config-local.php'))) {
			$configFile = (array) include_once(Path::join($cwd, 'config-local.php'));
		} elseif (Path::exists(Path::join($cwd, 'config-local.json'))) {
			$configFile = (array) json_decode(File::readContent(Path::join($cwd, 'config-local.json')), true);
		}
		
		if (! count($config) and ! count($configFile)) {
			throw new \InvalidArgumentException('No configuration defined');
		}
		
		$this->config = array_replace_recursive($configFile, $config);
	}
}
