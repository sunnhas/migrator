<?php
namespace Migrator\Utils;

class File
{
	/** @var resource */
	protected $handle;
	
	/** @var string */
	protected $path;
	
	/** @var string */
	protected $mode;
	
	
	public function __construct($path, $mode = 'r')
	{
		$this->path = $path;
		$this->mode = $mode;
		$this->handle = fopen($path, $mode);
	}
	
	
	public function __destruct()
	{
		$this->close();
	}
	
	
	/**
	 * @param string $path
	 *
	 * @return bool|File
	 */
	public static function create($path)
	{
		if (file_exists($path)) {
			return false;
		}
		
		return new self($path, 'w');
	}
	
	
	/**
	 * @param string $path
	 * @param string $mode
	 *
	 * @return bool|File
	 */
	public static function load($path, $mode = 'r')
	{
		if (! file_exists($path)) {
			return false;
		}
		
		return new self($path, $mode);
	}
	
	
	/**
	 * @param string $path
	 *
	 * @return bool|string
	 */
	public static function readContent($path)
	{
		$file = new self($path, 'r');
		
		$data = $file->read();
		
		$file->close();
		
		return $data;
	}
	
	
	/**
	 * @param string|mixed $data
	 */
	public function write($data)
	{
		if ($this->isWritable() or $this->isAppendable()) {
			fwrite($this->handle, $data);
		}
	}
	
	
	/**
	 * @param null|int $length
	 *
	 * @return bool|string
	 */
	public function read($length = null)
	{
		if ($length === null) {
			$length = filesize($this->path);
		}
		
		return fread($this->handle, $length);
	}
	
	
	public function delete()
	{
		$this->close();
		@unlink($this->path);
	}
	
	
	/**
	 * @return array
	 */
	public function stat()
	{
		return fstat($this->handle);
	}
	
	
	/**
	 * @return bool
	 */
	public function isWritable()
	{
		return (strpos($this->mode, 'w') !== false);
	}
	
	
	/**
	 * @return bool
	 */
	public function isAppendable()
	{
		return (strpos($this->mode, 'a') !== false);
	}
	
	
	/**
	 * 
	 */
	public function close()
	{
		@fclose($this->handle);
	}
}
