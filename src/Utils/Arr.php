<?php
namespace Migrator\Utils;

class Arr
{
	/**
	 * @param array      $array
	 * @param string     $key
	 * @param null|mixed $default
	 *
	 * @return null|mixed
	 */
	public static function get(array $array, $key, $default = null)
	{
		return (is_array($array) and array_key_exists($key, $array)) ? $array[$key] : $default;
	}
	
	
	/**
	 * @param array $array
	 * @param int   $sorting
	 */
	public static function filterFiles(array &$array, $sorting = SCANDIR_SORT_ASCENDING)
	{
		if (isset($array['.'])) {
			unset($array['.']);
		}
		
		if (isset($array['..'])) {
			unset($array['..']);
		}
		
		if ($sorting === SCANDIR_SORT_ASCENDING) {
			unset($array[0]);
			unset($array[1]);
		} elseif ($sorting === SCANDIR_SORT_DESCENDING) {
			$length = count($array) - 1;
			unset($array[$length]);
			unset($array[$length - 1]);
		}
	}
}
