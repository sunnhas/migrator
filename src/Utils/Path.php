<?php
namespace Migrator\Utils;

class Path
{
	/**
	 * @param string $path
	 *
	 * @return bool
	 */
	public static function exists($path)
	{
		return realpath($path) ? true : false;
	}
	
	
	/**
	 * @param string $path
	 *
	 * @return string
	 */
	public static function pretty($path)
	{
		if (self::exists($path)) {
			return realpath($path);
		} else {
			return $path;
		}
	}
	
	
	/**
	 * @param array ...$parts
	 *
	 * @return string
	 */
	public static function join(...$parts)
	{
		return implode(DIRECTORY_SEPARATOR, $parts);
	}
}
