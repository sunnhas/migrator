<?php
namespace Migrator\Command;

use Migrator\Command;

class UpCommand extends Command
{
	protected $commandName = 'up';
	protected $commandDescription = 'Run migration up';
	
	
	protected function handle()
	{
		$migrationsToExecute = $this->getMigrationsToUp();
		
		if ($migrationsToExecute) {
			foreach ($migrationsToExecute as $migration) {
				$this->executeUpMigration($migration);
				$this->output->writeln('Migrated: '.$migration->getFile());
			}
			
			$this->output->writeln('Database has been migrated');
		} else {
			$this->output->writeln('Database is up to date');
		}
	}
}
