<?php
namespace Migrator\Command;

use Migrator\Command;
use Migrator\Utils\File;
use Migrator\Utils\Path;
use Symfony\Component\Console\Input\InputArgument;

class CreateCommand extends Command
{
	protected $commandName = 'create';
	protected $commandDescription = 'Create new migration template';
	
	
	protected function configure()
	{
		parent::configure();
		
		$this->addArgument('description', InputArgument::REQUIRED, 'Description of the new migrations');
	}
	
	
	protected function handle()
	{
		$migrations = $this->getLocalMigrations();
		
		$id = count($migrations) + 1;
		$description = str_replace('_', '-', $this->input->getArgument('description'));
		
		$filePath = Path::join($this->cwd, 'migrations', $id.'_'.$description.'.sql');
		$file = File::create(Path::join($this->cwd, 'migrations', $id.'_'.$description.'.sql'));
		
		$stub = File::load(Path::pretty(__DIR__.'/../stubs/migration.tpl.sql'));
		
		$file->write($stub->read());
		
		$this->output->writeln('Migrations script has been created: '.$filePath);
	}
}
