<?php
namespace Migrator\Command;

use Migrator\Command;
use Migrator\Migration;
use Symfony\Component\Console\Helper\Table;

class StatusCommand extends Command
{
	protected $commandName = 'status';
	protected $commandDescription = 'Migration status';
	
	
	public function handle()
	{
		$table = new Table($this->output);
		$table->setHeaders(Migration::toArrayHeaders());
		
		$migrations = $this->getLocalAndRemoteMigrations();
		
		foreach ($migrations as $migration) {
			$table->addRow($migration->toArray());
		}
		
		$table->render();
	}
}
