<?php
namespace Migrator\Command;

use Migrator\Command;

class InitCommand extends Command
{
	protected $commandName = 'init';
	protected $commandDescription = 'Initialize migration changelog';
	protected $commandHelp = 'help';
	
	
	protected function handle()
	{
		$changelogTable = 'changelog';
		
		$this->getDatabase()->run("
            CREATE TABLE $changelogTable
            (
                id numeric(20,0),
                applied_at character varying(25),
                version character varying(25),
                description character varying(255)
            )
        ");
		
		$this->output->writeln('Migrator has been initialized with table: '.$changelogTable);
	}
}
