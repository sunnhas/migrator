<?php
namespace Migrator\Command;

use Migrator\Command;

class DownCommand extends Command
{
	protected $commandName = 'down';
	protected $commandDescription = 'Take down migrations';
	
	
	protected function handle()
	{
		$migrationsToExecute = $this->getRemoteMigrations();
		if (! $migrationsToExecute) {
			$this->output->writeln('No migrations to take down');
			
			return;
		}
		
		foreach ($migrationsToExecute as $migration) {
			$this->executeDownMigration($migration);
			$this->output->writeln('Deleted migration: '.$migration->getFile());
		}
		
		$this->output->writeln('Database is now empty');
	}
}
